// Класс Employee
class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    // Геттеры
    get name() {
        return this._name;
    }

    get age() {
        return this._age;
    }

    get salary() {
        return this._salary;
    }

    // Сеттеры
    set name(name) {
        this._name = name;
    }

    set age(age) {
        this._age = age;
    }

    set salary(salary) {
        this._salary = salary;
    }
}

// Класс Programmer, который наследует Employee
class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    // Перезапись геттера для свойства salary
    get salary() {
        return super.salary * 3; // Використовуємо геттер salary з класу Employee і множимо на 3
    }

    // Геттер и сеттер для свойства lang
    get lang() {
        return this._lang;
    }

    set lang(lang) {
        this._lang = lang;
    }
}

// Создание экземпляров класса Programmer
const programmer1 = new Programmer('Alice', 30, 5000, ['JavaScript', 'Python']);
const programmer2 = new Programmer('Bob', 25, 4000, ['Java', 'C++']);

// Вывод экземпляров в консоль
console.log(`Programmer 1: Name = ${programmer1.name}, Age = ${programmer1.age}, Salary = ${programmer1.salary}, Languages = ${programmer1.lang.join(', ')}`);
console.log(`Programmer 2: Name = ${programmer2.name}, Age = ${programmer2.age}, Salary = ${programmer2.salary}, Languages = ${programmer2.lang.join(', ')}`);
